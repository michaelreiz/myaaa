var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var violationSchema = new Schema({
    blockedUri: String,
    documentUri: String,
    isEnforced: Boolean,
    violatedDirective: String,
    policy: String,
    date: Date
});

module.exports = mongoose.model('Violation', violationSchema );
